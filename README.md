# TV Controller

A script based TV controller that was written for my __WINTV-PVR-150__ card to easily set channel and volume.
Video device (defined in _conf/global.conf_) is automatically opened by _[VLC](http://www.videolan.org/vlc/)_.

### Dependencies

The following packages should be installed:

* v4l-utils
* vlc
* jq

### How to use

1. Create your own _channels.json_ based on your service provider's frequency table.
2. Make _tv_ executable and execute it:

   ```bash
   $ chmod +x tv
   $ ./tv
   ```
3. Use the predefined keys to change channel/volume.
   Keys are defined in _conf/keys.conf_, default configuration is

   * Channel up: _w_
   * Channel down: _s_
   * Volume up: _d_
   * Volume down: _a_
