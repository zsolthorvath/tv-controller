#!/bin/bash
#
# Volume related functions.
#
# Author:
#   Zsolt Horvath

VOLUME_STEP=655

# Get volume.
#
# Globals:
#   VIDEO_DEVICE
#   VOLUME_STEP
# Arguments:
#   None
# Returns:
#   The current volume
function get_volume() {
    echo $(( $(v4l2-ctl --device $VIDEO_DEVICE --get-ctrl volume | awk '{print $2}') / $VOLUME_STEP ))
}

# Set volume.
#
# Globals:
#   VIDEO_DEVICE
#   VOLUME_STEP
# Arguments:
#   $1: Volume value to be set
# Returns:
#   None
function set_volume() {
    local volume=$1
    v4l2-ctl --device $VIDEO_DEVICE --set-ctrl volume=$(( $volume * $VOLUME_STEP ))
}

# Print the current volume then move the cursor back to the beginning of the line.
#
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   Current volume
function print_volume() {
    echo -ne \\033[K"Volume: $(get_volume)%"\\r
}
