#!/bin/bash
#
# Channel related functions.
#
# Author:
#   Zsolt Horvath

# Get number of channels.
#
# Globals:
#   CHANNELS
# Arguments:
#   None
# Returns:
#   Number of channels
function get_number_of_channels() {
    cat $CHANNELS | jq ".channels" | jq "length"
}

# Set channel.
#
# Globals:
#   VIDEO_DEVICE
# Arguments:
#   $1: Channel number
# Returns:
#   None
function set_channel() {
    local channel_number=$1
    v4l2-ctl --device $VIDEO_DEVICE --set-freq $(__get_channel_freq $channel_number) &> /dev/null
}

# Print channel name based on the provided channel number
# then move the cursor back to the beginning of the line.
#
# Globals:
#   None
# Arguments:
#   $1: Channel number
# Returns:
#   Channel name
function print_channel_name() {
    local channel_number=$1
    echo -ne \\033[K"$(__get_channel_name $channel_number)"\\r
}

# Print list of channels.
#
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   List of channels
function print_channels() {
    # Determine the longest channel name and use its length as column width in table of channels
    # (cannot be a local variable since it is used by the function '__print_channel')
    COLUMN_WIDTH=$(echo "$(__get_channel_name 1)" | wc -c)

    local i=2
    while [[ i -le $(get_number_of_channels) ]]; do
        if [[ COLUMN_WIDTH -lt $(echo "$(__get_channel_name $i)" | wc -c) ]]; then
            COLUMN_WIDTH=$(echo "$(__get_channel_name $i)" | wc -c)
        fi
        (( i += 1 ))
    done

    # Print table of channel names
    local columns=2
    local rows=$(( ($(get_number_of_channels) + columns - 1) / columns ))

    local row=0
    while [[ row -lt rows ]]; do
        local column=0
        while [[ column -lt columns ]]; do
            local channel_number=$(( (row * columns) + column + 1 ))
            if [[ channel_number -le $(get_number_of_channels) ]]; then
                __print_channel $channel_number
            fi
            (( column += 1 ))
        done

        echo -ne "\n"
        (( row += 1 ))
    done
}

# Print channel name based on the provided channel number
# with proper number of trailing space characters.
#
# Globals:
#   None
# Arguments:
#   $1: Channel number
# Returns:
#   Formatted channel name
function __print_channel() {
    local channel_number=$1
    echo -n "$(__get_channel_name $channel_number)"

    local spaces=$(( COLUMN_WIDTH - $(echo "$(__get_channel_name $channel_number)" | wc -c) + 1 ))

    local i=0
    while [[ i -lt spaces ]]; do
        echo -n " "
        (( i += 1 ))
    done
}

# Get name and number of the specified channel.
#
# Globals:
#   CHANNELS
# Arguments:
#   $1: Channel number
# Returns:
#   Name and number of the specified channel
function __get_channel_name() {
    local channel_number=$1
    echo "#$channel_number $(cat $CHANNELS | jq -r ".channels[$(( channel_number - 1 ))].name")"
}

# Get frequency of the specified channel.
#
# Globals:
#   CHANNELS
# Arguments:
#   $1: Channel number
# Returns:
#   Frequency of the specified channel
function __get_channel_freq() {
    local channel_number=$1
    cat $CHANNELS | jq -r ".channels[$(( channel_number - 1 ))].freq"
}
